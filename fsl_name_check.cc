/*  fsl_name_check.cc

    Mark Jenkinson, FMRIB Image Analysis Group

    Copyright (C) 2003 University of Oxford  */

/*  CCOPYRIGHT  */

// Check potential subject IDs to see if they contain names, initials, enough numbers, etc.
//  to adhere to FMRIB policy (this is not guaranteed to find all possible invalid names,
//  but should do a large percentage)

#define _GNU_SOURCE 1
#define POSIX_SOURCE 1

#include <cctype>
#include <string>
#include <fstream>
#include <ios>
#include <iostream>
#include <vector>
#include <algorithm>
#include "utils/options.h"

using namespace std;
using namespace Utilities;

// The two strings below specify the title and example usage that is
//  printed out as the help or usage message

string title="fsl_name_check (Version 1.0)\nCopyright(c) 2008, University of Oxford (Mark Jenkinson)\nCheck to see if subject ID conforms to FMRIB policy\n";
string examples="fsl_name_check -s subjid -f disallowed_names -e exceptions";

// Each (global) object below specificies as option and can be accessed
//  anywhere in this file (since they are global).  The order of the
//  arguments needed is: name(s) of option, default value, help message,
//       whether it is compulsory, whether it requires arguments
// Note that they must also be included in the main() function or they
//  will not be active.

Option<bool> verbose(string("-v,--verbose"), false,
		     string("switch on diagnostic messages"),
		     false, no_argument);
Option<bool> noinitials(string("-n,--noinitials"), false,
		  string("turn off test for initials"),
		  false, no_argument);
Option<bool> message(string("-m,--message"), false,
		  string("display the naming policy message only"),
		  false, no_argument);
Option<bool> debug(string("-d,--debug"), false,
		  string("turn on debugging output"),
		  false, no_argument);
Option<bool> help(string("-h,--help"), false,
		  string("display this message"),
		  false, no_argument);
Option<string> subjid(string("-s,--subjectid"), string(""),
		  string("input subject ID string"),
		  false, requires_argument);
Option<string> filename(string("-f,--file"), string(""),
		  string("filename of file containing list of disallowed names"),
		  false, requires_argument);
Option<string> exceptions(string("-e,--exceptions"), string(""),
		  string("filename of file containing list of excepted (allowed) names"),
		  false, requires_argument);
int nonoptarg;

////////////////////////////////////////////////////////////////////////////

// Local functions

char mytolower(char c)
{
  return (char) tolower((int) c);
}

int tolower(string& data) {
  std::transform(data.begin(), data.end(), data.begin(), mytolower);
  return 0;
}

void policy_message() {
  cout << " " << endl;
  cout << "It is FMRIB policy and compulsory under the GCP that all filenames should be anonymised." << endl;
  cout << " " << endl;
  cout << "This precludes the use of first names, last names _and_ initials." << endl;
  cout << "The preferred naming should use anonymised, unique, numerical identifiers" << endl;
  cout << "(e.g. Patient_XXXX) and you should store the relationship between these" << endl;
  cout << "identifiers and other subject information in a separate file on the server only -"<< endl;
  cout << "not on any laptops, external drives or externally accessible places (e.g. webpages)." << endl;
  cout << " " << endl;
}

int do_work(int argc, char* argv[])
{
  if (message.value()) { policy_message(); return 0; }

  bool valid=true;
  string name="", fname, sname;
  size_t loc;

  sname = subjid.value();


  // check that at least two consecutive digits occur
  int consec=0, maxconsec=0;
  for (size_t n=0; n<sname.length(); n++) {
    if (isdigit(sname[n])) { consec++; maxconsec=max(consec,maxconsec); }
    else consec=0;
  }
  if (maxconsec<2) {
    if (verbose.value()) {
      cout << "Could not find two or more consecutive digits in " << subjid.value() << endl;
    }
    valid=false;
  }
  string snameorig=sname;
  tolower(sname);


  // remove words that are OK: patient, pat, control, con
  if (exceptions.set()) {
    fname=exceptions.value();
    ifstream fs(fname.c_str());
    if (!fs.good()) {
      cerr << "Error: could not open file " << filename << endl;
      exit(EXIT_FAILURE);
    }
    while (!fs.eof()) {
      fs >> name;
      tolower(name);
      if (!fs.eof()) {
	loc = sname.find(name);
	if (loc != string::npos) {
	  if (debug.value()) {
	    cout << "Erasing " << name << " from " << sname << endl;
	  }
	  sname.erase(loc,name.length());
	  snameorig.erase(loc,name.length());
	  if (debug.value()) { cout << "Post-erasure: " << sname << endl; }
	}
      }
    }
    fs.close();
  }


  // check for 2 or 3 letter initials (same case surrounded by non-alphabetic)
  if (!noinitials.value()) {
    string initials="", initest="";
    for (size_t n=0; n<snameorig.length(); n++) {
      if (isalpha(snameorig[n])) {
	if (initest.length()==0) {
	  initest = snameorig[n];
	} else {
	  if (islower(initest[initest.length()-1])==islower(snameorig[n])) {
	    initest += snameorig[n];
	  } else {
	    initest="";
	  }
	}
      } else {
	if ((initest.length()==2) || (initest.length()==3)) {
	  initials=initest;
	}
	initest="";
      }
    }
    if ( (initials.length()==0) && ( (initest.length()==2) || (initest.length()==3)) ) {
      initials=initest;
    }
    if (initials.length()>0) {
      if (verbose.value()) {
	cout << "Found initials " << initials << " in " << subjid.value() << endl;
      }
      valid=false;
    }
  }


  // open up the file and check for all the names
  if (filename.set()) {
    fname=filename.value();
    ifstream fs(fname.c_str());
    if (!fs.good()) {
      cerr << "Error: could not open file " << filename << endl;
      exit(EXIT_FAILURE);
    }
    while (!fs.eof()) {
      fs >> name;
      if (!fs.eof()) {
	tolower(name);
	if (debug.value()) { cout << "Testing: " << name << endl; }
	loc = sname.find(name);
	if (loc!=string::npos) {
	  if (verbose.value()) {
	    cout << "Found the name " << name << " in " << subjid.value() << endl;
	  }
	  valid=false;
	}
      }
    }
    fs.close();
  }

  if (valid) {
    cout << "VALID" << endl;
  } else {
    cout << "INVALID" << endl;
    if (verbose.value()) { policy_message(); }
  }
  return 0;
}

////////////////////////////////////////////////////////////////////////////

int main(int argc,char *argv[])
{

  OptionParser options(title, examples);

  try {
    // must include all wanted options here (the order determines how
    //  the help message is printed)
    options.add(subjid);
    options.add(filename);
    options.add(exceptions);
    options.add(noinitials);
    options.add(message);
    options.add(verbose);
    options.add(debug);
    options.add(help);

    nonoptarg = options.parse_command_line(argc, argv);

    // line below stops the program if the help was requested or
    //  a compulsory option was not set
    if ( (help.value()) || (!options.check_compulsory_arguments(true)) || (message.unset() && subjid.unset()))
      {
	options.usage();
	exit(EXIT_FAILURE);
      }

  }  catch(X_OptionError& e) {
    options.usage();
    cerr << endl << e.what() << endl;
    exit(EXIT_FAILURE);
  } catch(std::exception &e) {
    cerr << e.what() << endl;
  }

  // Call the local functions

  return do_work(argc,argv);
}
