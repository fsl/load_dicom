#
# LoadDICOM - the GUI for DICOM to NIfTI conversion
#
# Mark Jenkinson, FMRIB Image Analysis Group
#
# Copyright (C) 2008 University of Oxford
#
# TCLCOPYRIGHT

source [ file dirname [ info script ] ]/fslstart.tcl


proc loaddicom { w } {

    global entries FSLDIR PWD subjid

    catch { exec sh -c "hostname | sed 's/.fmrib.ox.ac.uk//'" } fullans
    if { $fullans == "jalapeno" } {
	puts "Please do not run LoadDICOM on jalapeno."
	puts "Use your local desktop machine instead."
	exit 1
    }

    # ---- Set up Frames ----
    toplevel $w
    wm title $w "LoadDICOM"
    wm iconname $w "LoadDICOM"
    wm iconbitmap $w @${FSLDIR}/tcl/fmrib.xbm
    frame $w.f

    TitleFrame $w.f.input -text "Input"  -relief groove
    set lfinput [ $w.f.input getframe ]

    # Input image

    set entries(inpath) "/media/"
    catch { exec sh -c "uname" } uname
    if { $uname == "Darwin" } {
	set entries(inpath) "/Volumes/"
    }

    FileEntry  $w.f.inpath -textvariable entries(inpath) -label "Input Path (for DICOMs)    " -title "Select" -width 40 -filedialog directory  -filetypes IMAGE

    pack $w.f.inpath -in $lfinput -side top -anchor w -pady 3 -padx 5

    # Output frame

    TitleFrame $w.f.output -text "Output" -relief groove
    set lfoutput [ $w.f.output getframe ]

    # subject id

    set subjid ""
    set entries(check) 1

    frame $w.f.subjid
    label $w.f.subjid.text -text "Subject ID (to be appended to the output path) "
    entry $w.f.subjid.in -textvariable subjid -width 23
    bind $w.f.subjid.in <FocusOut> { loaddicom:checksubjid }
    pack $w.f.subjid.text $w.f.subjid.in -in $w.f.subjid -side left -anchor w

    # subject id checkbox


    frame $w.f.check
    checkbutton $w.f.check.button -text "Check Subject ID " -variable entries(check) -command "loaddicom:checkbox "
    pack $w.f.check.button -in $w.f.check -side left -padx 3 -pady 3 -anchor w

    # output path

    set entries(outpath) ""
    FileEntry  $w.f.outpath -textvariable entries(outpath) -label "Output Path                        " -title "Select" -width 40 -filedialog directory  -filetypes IMAGE

    # pack

    pack $w.f.subjid $w.f.check $w.f.outpath -in $lfoutput -side top -anchor w -pady 3 -padx 5

    pack $w.f.input $w.f.output -in $w.f -side top -anchor w -pady 0 -padx 5

    # ---- Button Frame ----

    frame $w.btns
    frame $w.btns.b -relief raised -borderwidth 1

    button $w.apply     -command "loaddicom:apply " \
	    -text "Go" -width 5

    button $w.cancel    -command "destroy $w" \
	    -text "Exit" -width 5

    button $w.help -command "FmribWebHelp file: ${FSLDIR}/doc/redirects/flirt.html" \
	    -text "Help" -width 5

    pack $w.btns.b -side bottom -fill x
    pack $w.apply $w.cancel $w.help -in $w.btns.b \
	    -side left -expand yes -padx 3 -pady 10 -fill y

    pack $w.f $w.btns -expand yes -fill both
}

proc loaddicom:checkbox { } {
    global FSLDIR entries subjid
    update idletasks
    if { $entries(check) == 0 } {
	set count 0
	set w0 ".dialog[incr count]"
	while { [ winfo exists $w0 ] } {
	    set w0 ".dialog[incr count]"
	}

	toplevel $w0
	wm title $w0 "FMRIB Naming Policy"
	wm iconname $w0 "Policy"
	wm iconbitmap $w0 @${FSLDIR}/tcl/fmrib.xbm

	catch { exec sh -c "${FSLDIR}/bin/fsl_name_check -m" } fullans
	set chkwarning "You have turned off the Subject ID checking function!\nPlease be aware that you need to abide by the FMRIB Naming Policy:"
	set chkmessage $fullans
	label $w0.msg1 -text "$chkwarning" -font {Helvetica 12 bold} -foreground red
	label $w0.msg2 -text "$chkmessage" -font {Helvetica 12 bold} -foreground black
	button $w0.cancel -command "destroy $w0" -text "OK"
	pack $w0.msg1 $w0.msg2 $w0.cancel -in $w0
    } else {
	loaddicom:checksubjid
    }
}


proc loaddicom:checksubjid { } {
    global FSLDIR entries subjid
    # puts "TESTING SUBJECT ID $subjid"
    set valid 1
    if { $entries(check) == 1 } {
	if { $subjid != "" } {
	    catch { exec sh -c "${FSLDIR}/bin/fsl_name_check -s $subjid -f ${FSLDIR}/data/load_dicom/names.disallowed -e ${FSLDIR}/data/load_dicom/names.exceptions" } ans
	    if { $ans == "INVALID" } {
		set valid 0
		set count 0
		set w0 ".dialog[incr count]"
		while { [ winfo exists $w0 ] } {
		    set w0 ".dialog[incr count]"
		}

		toplevel $w0
		wm title $w0 "Subject Name Warning"
		wm iconname $w0 "Warning"
		wm iconbitmap $w0 @${FSLDIR}/tcl/fmrib.xbm

		catch { exec sh -c "${FSLDIR}/bin/fsl_name_check -s $subjid -f ${FSLDIR}/data/load_dicom/names.disallowed -e ${FSLDIR}/data/load_dicom/names.exceptions -v | sed 's/INVALID//' | uniq" } fullans
		set chkwarning "WARNING - BAD SUBJECT ID ENTRY"
		set chkmessage $fullans
		label $w0.msg1 -text "$chkwarning" -font {Helvetica 12 bold} -foreground red
		label $w0.msg2 -text "$chkmessage" -font {Helvetica 12 bold} -foreground black
		button $w0.cancel -command "destroy $w0" -text "Dismiss"
		pack $w0.msg1 $w0.msg2 $w0.cancel -in $w0

	    }
	}
    }
    update idletasks
    return $valid
}

proc loaddicom:apply { } {
    global entries subjid

    set valid [ loaddicom:checksubjid ]

    if { $valid == 1 } {
	set status [ loaddicom:proc $entries(inpath) $subjid $entries(outpath) ]

	update idletasks
    }
}



proc loaddicom:proc { inpath subjid outpath } {

    global FSLDIR entries
    set checkcomm ""
    if { $entries(check) == 0 } { set checkcomm "-u" }

    set thecommand "${FSLDIR}/bin/load_dicom -s $subjid -i $inpath -o $outpath $checkcomm"
    catch { exec sh -c "$thecommand" } msg

    return 0
}



wm withdraw .
loaddicom .rename
tkwait window .rename
