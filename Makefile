# A Makefile for the image class

include ${FSLCONFDIR}/default.mk

PROJNAME  = load_dicom
XFILES    = fsl_name_check
SCRIPTS   = load_dicom
RUNTCLS   = LoadDICOM
DATAFILES = names.disallowed names.exceptions
LIBS      = -lfsl-utils

all: ${XFILES}

fsl_name_check: fsl_name_check.o
	$(CXX) ${CXXFLAGS} -o $@ $^ ${LDFLAGS}
